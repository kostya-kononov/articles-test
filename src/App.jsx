/* eslint-disable react/no-array-index-key */
import React from 'react';
import { Switch, Route } from 'react-router-dom';
import routes from 'helpers/routing';
import { Provider } from 'react-redux';
import store from 'store';

const App = () => (
  <Provider store={store}>
    <main>
      <Switch>
        {Object.values(routes).map(({ path, exact = false, component: Module }, key) => (
          <Route
            key={key}
            path={path}
            exact={exact}
            component={(props) => (
              <Module
                {...props}
                routes={routes}
              />
            )}
          />
        ))}
      </Switch>
    </main>
  </Provider>
);

export default App;

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import ArticleForm from 'components/ArticleForm';
import LocaleWrapper from 'components/LocaleWrapper';
import { addArticle, editArticle } from 'store/articles/actions';

const AddArticle = (props) => {
  const {
    addArticle,
    editArticle,
    locale,
    history,
    location,
    routes,
    match,
    articles,
  } = props;

  const getArticleById = (id) => {
    if (!id) return null;

    return articles.find((item) => item.id === id);
  };

  const getPageTitle = (article) => {
    if (!article) return 'Add New Article';

    const { locales } = article;
    return `Edit Article: ${locales[locale].title}`;
  };

  const { id: articleId } = match.params;
  const article = getArticleById(articleId);
  return (
    <div className="container pt-4">
      <h1 className="h2">{getPageTitle(article)}</h1>
      <ArticleForm
        article={article}
        locale={locale}
        history={history}
        location={location}
        addArticle={addArticle}
        editArticle={editArticle}
        routes={routes}
      />
    </div>
  );
};

AddArticle.propTypes = {
  addArticle: PropTypes.func.isRequired,
  editArticle: PropTypes.func.isRequired,
  locale: PropTypes.oneOf(['en', 'de', 'bg']),
  history: PropTypes.shape({
    push: PropTypes.func,
    replace: PropTypes.func,
  }).isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
  routes: PropTypes.object.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }).isRequired,
  articles: PropTypes.array.isRequired,
};

AddArticle.defaultProps = {
  locale: 'en',
};

const mapStateToProps = ({ articles }) => ({
  articles: articles.list,
});

const mapDispatchToProps = {
  addArticle,
  editArticle,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LocaleWrapper(AddArticle));

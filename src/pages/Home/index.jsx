import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import ArticleCard from 'components/ArticleCard';
import Header from 'components/Header';
import LocaleWrapper from 'components/LocaleWrapper';

const HomePage = (props) => {
  const {
    locale,
    articles,
    history,
    location,
  } = props;

  return (
    <>
      <Header
        locale={locale}
        history={history}
        location={location}
      />
      <main className="container">
        <h1 className="h4 text-center mb-4">Articles Listing</h1>
        <div>
          {articles.map((item) => {
            const {
              id,
              date,
              isActive,
              locales,
            } = item;

            return (
              <ArticleCard
                key={id}
                id={id}
                date={date}
                isActive={isActive}
                title={locales[locale].title}
                content={locales[locale].content}
                preview={locales[locale].preview}
                locale={locale}
              />
            );
          })}
        </div>
      </main>
    </>
  );
};

HomePage.propTypes = {
  locale: PropTypes.oneOf(['en', 'de', 'bg']),
  articles: PropTypes.array.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
    replace: PropTypes.func,
  }).isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
};

HomePage.defaultProps = {
  locale: 'en',
};

const mapStateToProps = ({ articles }) => ({
  articles: articles.list,
});

export default connect(mapStateToProps)(LocaleWrapper(HomePage));

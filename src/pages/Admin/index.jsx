import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Button, Table } from 'react-bootstrap';

import Header from 'components/Header';
import LocaleWrapper from 'components/LocaleWrapper';
import { getDateShortString } from 'helpers/date';
import { getEditArticleUri, getLocaleURL } from 'helpers/locale';
import DeleteConfirmation from 'components/DeleteConfirmation';
import { deleteArticle } from 'store/articles/actions';
import { Links } from 'constants/global';

const AdminPage = (props) => {
  const [isDeleteVisible, setDeleteVisible] = useState(false);
  const [articleId, setArticleId] = useState(null);
  const {
    locale,
    articles,
    history,
    location,
    deleteArticle,
  } = props;

  const onDeleteClicked = (articleId) => {
    setDeleteVisible(true);
    setArticleId(articleId);
  };

  const onCloseModal = () => {
    setDeleteVisible(false);
  };

  const onDeleteConfirmed = () => {
    if (!articleId) return;

    deleteArticle(articleId);
    setArticleId(null);
    onCloseModal();
  };

  return (
    <>
      <Header
        locale={locale}
        history={history}
        location={location}
      />
      <main className="container">
        <div className="d-flex justify-content-end mb-2">
          <Link
            className="text-accent text-decoration-underline"
            to={getLocaleURL(Links.ADD_ARTICLE, locale)}
          >
            Add new article
          </Link>
        </div>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Date</th>
              <th>Title</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {articles.map((item) => {
              const { id, locales, date } = item;
              const editUri = getEditArticleUri(id, locale);

              return (
                <tr key={id}>
                  <td>{getDateShortString(new Date(date))}</td>
                  <td>{locales[locale].title}</td>
                  <td>
                    <Link to={editUri}>Edit</Link>
                    {' | '}
                    <Button
                      variant="link p-0 text-dark button-link"
                      onClick={() => onDeleteClicked(id)}
                    >
                      Delete
                    </Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
        <DeleteConfirmation
          visible={isDeleteVisible}
          onSubmit={onDeleteConfirmed}
          onClose={onCloseModal}
        />
      </main>
    </>
  );
};

AdminPage.propTypes = {
  locale: PropTypes.oneOf(['en', 'de', 'bg']),
  articles: PropTypes.array.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
    replace: PropTypes.func,
  }).isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
  deleteArticle: PropTypes.func.isRequired,
};

AdminPage.defaultProps = {
  locale: 'en',
};

const mapStateToProps = ({ articles }) => ({
  articles: articles.list,
});

export default connect(
  mapStateToProps,
  { deleteArticle },
)(LocaleWrapper(AdminPage));

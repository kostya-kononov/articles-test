/* eslint-disable react/no-danger */
import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import LocaleWrapper from 'components/LocaleWrapper';
import { connect } from 'react-redux';
import DOMPurify from 'dompurify';

import BackButton from 'components/BackButton';
import Header from 'components/Header';
import Pagination from 'components/Pagination';
import { Links } from 'constants/global';
import { getDateString } from 'helpers/date';
import { getLocaleURL } from 'helpers/locale';

const ViewArticle = (props) => {
  const {
    locale,
    articles,
    match,
    history,
    location,
  } = props;
  const { id } = match.params;
  const item = articles.find((val) => val.id === id);
  if (!item) {
    return <Redirect to={getLocaleURL(Links.HOME, locale)} />;
  }

  const { title, content } = item.locales[locale];
  const dateString = getDateString(new Date(item.date));
  const innerHTML = { __html: DOMPurify.sanitize(content) };
  return (
    <>
      <Header
        locale={locale}
        history={history}
        location={location}
      />
      <main className="container">
        <h1 className="h4 text-center">Article Details</h1>
        <h3 className="h3 text-center text-accent">{title}</h3>
        <div dangerouslySetInnerHTML={innerHTML} />
        <p className="text-secondary d-flex">
          { dateString }
        </p>
        <BackButton
          text="Back to the listing"
          to={getLocaleURL(Links.HOME, locale)}
        />
        <Pagination articles={articles} currentId={id} locale={locale} />
        <div className="mt-3" />
      </main>
    </>
  );
};

ViewArticle.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }).isRequired,
  locale: PropTypes.oneOf(['en', 'de', 'bg']),
  articles: PropTypes.array.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
    replace: PropTypes.func,
  }).isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
};

ViewArticle.defaultProps = {
  locale: 'en',
};

const mapStateToProps = ({ articles }) => ({
  articles: articles.list,
});

export default connect(mapStateToProps)(LocaleWrapper(ViewArticle));

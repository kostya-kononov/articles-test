/* eslint-disable import/prefer-default-export */
export const LOCALES = [
  {
    title: 'English',
    shortHand: 'en',
  },
  {
    title: 'German',
    shortHand: 'de',
  },
  {
    title: 'Bulgarian',
    shortHand: 'bg',
  },
];

export const AppLocale = {
  English: 'en',
  German: 'de',
  Bulgarian: 'bg',
};

const { PUBLIC_URL } = process.env;
export const Links = {
  HOME: `${PUBLIC_URL}/:lang/articles`,
  ADMIN: `${PUBLIC_URL}/admin/:lang/articles`,
  ADD_ARTICLE: `${PUBLIC_URL}/admin/:lang/articles/add`,
  VIEW_ARTICLE: `${PUBLIC_URL}/:lang/articles/:id`,
  EDIT_ARTICLE: `${PUBLIC_URL}/admin/:lang/articles/edit/:id`,
};

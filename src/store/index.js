import {
  applyMiddleware, combineReducers, compose, createStore,
} from 'redux';
import thunk from 'redux-thunk';
import articles from './articles/reducer';

const rootReducer = combineReducers({ articles });
const store = createStore(
  rootReducer,
  compose(applyMiddleware(...[thunk])),
);

export default store;

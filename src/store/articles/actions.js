import hash from 'object-hash';
import { SET_ARTICLES } from './actionTypes';

export const setArticles = (payload) => ({
  type: SET_ARTICLES,
  payload,
});

export const addArticle = (article) => async (dispatch, getStore) => {
  const { articles: { list } } = getStore();
  const { localStorage } = window;

  const articles = [...list, {
    id: hash(article),
    ...article,
  }];
  localStorage.setItem('articles', JSON.stringify(articles));
  dispatch(setArticles(articles));
};

export const editArticle = (id, article) => async (dispatch, getStore) => {
  const { articles: { list } } = getStore();
  const { localStorage } = window;

  const articles = list.map((item) => {
    if (item.id === id) return article;
    return item;
  });
  localStorage.setItem('articles', JSON.stringify(articles));
  dispatch(setArticles(articles));
};

export const deleteArticle = (id) => async (dispatch, getStore) => {
  const { articles: { list } } = getStore();
  const { localStorage } = window;

  const articles = list.filter((item) => (item.id !== id));
  localStorage.setItem('articles', JSON.stringify(articles));
  dispatch(setArticles(articles));
};

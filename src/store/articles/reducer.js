import { SET_ARTICLES } from './actionTypes';

const getArticles = () => {
  const { localStorage } = window;
  const articles = localStorage.getItem('articles');
  return articles ? JSON.parse(articles) : [];
};

const initialState = {
  list: getArticles(),
};

const hanlders = {
  [SET_ARTICLES]: (state, action) => ({
    ...state,
    list: action.payload,
  }),
};

const ArticlesReducer = (state = initialState, action) => {
  if (hanlders[action.type]) {
    return hanlders[action.type](state, action);
  }
  return state;
};

export default ArticlesReducer;

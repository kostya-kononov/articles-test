/* eslint-disable react/no-array-index-key */
import React from 'react';
import PropTypes from 'prop-types';
import { LOCALES } from 'constants/global';
import styles from './LocaleSelect.module.css';

const LocaleSelect = (props) => {
  const { locale, history, location } = props;

  const onChange = (event) => {
    const { value } = event.target;
    const path = location.pathname.replace(`/${locale}/`, `/${value}/`);
    history.push(path);
  };

  return (
    <div>
      Lang:
      <select value={locale} className={styles.select} onChange={onChange}>
        {LOCALES.map((item, key) => (
          <option
            key={key}
            value={item.shortHand}
          >
            {item.title}
          </option>
        ))}
      </select>
    </div>
  );
};

LocaleSelect.propTypes = {
  locale: PropTypes.oneOf(['en', 'de', 'bg']),
  history: PropTypes.shape({
    push: PropTypes.func,
    replace: PropTypes.func,
  }).isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
};

LocaleSelect.defaultProps = {
  locale: 'en',
};

export default LocaleSelect;

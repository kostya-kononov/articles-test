import React from 'react';
import PropTypes from 'prop-types';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Links } from 'constants/global';
import { getDateString } from 'helpers/date';

const ArticleCard = (props) => {
  const {
    id,
    title,
    preview,
    date,
    locale,
    isActive,
  } = props;

  if (!isActive) return null;

  const url = Links.VIEW_ARTICLE.replace(':lang', locale).replace(':id', id);
  const dateString = getDateString(new Date(date));
  return (
    <Card className="mb-2">
      <Card.Body>
        <Card.Title className="text-accent">
          <Link to={url}>{title}</Link>
        </Card.Title>
        <Card.Text>{ preview }</Card.Text>
        <Card.Subtitle className="mb-2 text-secondary">{ dateString }</Card.Subtitle>
      </Card.Body>
    </Card>
  );
};

ArticleCard.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  preview: PropTypes.string.isRequired,
  date: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.instanceOf(Date),
  ]).isRequired,
  locale: PropTypes.oneOf(['en', 'de', 'bg']),
  isActive: PropTypes.bool,
};

ArticleCard.defaultProps = {
  locale: 'en',
  isActive: true,
};

export default ArticleCard;

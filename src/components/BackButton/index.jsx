import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styles from './BackButton.module.css';

const BackButton = (props) => {
  const { text, to } = props;

  return (
    <div className={styles.wrap}>
      <div className={styles.link}>
        <i className="bi bi-arrow-left" />
        <Link to={to}>{text}</Link>
      </div>
    </div>
  );
};

BackButton.propTypes = {
  text: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired,
};

export default BackButton;

import * as yup from 'yup';
import { LOCALES } from 'constants/global';

const getLocales = () => LOCALES.reduce((total, current) => ({
  ...total,
  [current.shortHand]: yup.object({
    title: yup.string().required('Title should not be empty'),
    content: yup.string().required('Content should not be empty'),
  }),
}), {});

const getArticleSchema = () => yup.object({
  date: yup
    .date()
    .nullable()
    .required('Date should not be empty.'),
  isActive: yup.bool(),
  locales: yup.object(getLocales()),
});

const articleSchema = getArticleSchema();
export default articleSchema;

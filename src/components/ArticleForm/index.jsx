import React from 'react';
import PropTypes from 'prop-types';
import {
  Alert,
  Button,
  Col,
  Form,
  Row,
  Tab,
  Tabs,
} from 'react-bootstrap';
import ReactDatePicker from 'react-datepicker';
import { FieldArray, Formik } from 'formik';

import InputGroup from 'components/InputGroup';
import { LOCALES, Links } from 'constants/global';
import { getArticleUri, getLocaleURL } from 'helpers/locale';

import 'react-datepicker/dist/react-datepicker.css';
import ArticleBody from './Body';
import schema from './schema';

const ArticleForm = (props) => {
  const {
    addArticle,
    editArticle,
    locale,
    history,
    location,
    article: originalArticle,
  } = props;

  const getInitialValues = () => {
    if (originalArticle) {
      return {
        ...originalArticle,
        date: new Date(originalArticle.date),
      };
    }

    const localizedData = LOCALES.reduce((total, current) => ({
      ...total,
      [current.shortHand]: {
        title: '',
        content: '',
        preview: '',
      },
    }), {});

    return {
      locales: localizedData,
      date: null,
      isActive: false,
    };
  };

  const onSwitchTabs = (key) => {
    const path = location.pathname.replace(`/${locale}/`, `/${key}/`);
    history.push(path);
  };

  const getTabClass = (errors, lang, currentLocale) => {
    const isActiveTab = currentLocale === lang;
    if (isActiveTab) {
      return '';
    }

    return (errors.locales && errors.locales[lang] ? 'text-danger' : '');
  };

  const handleSubmit = (article) => {
    if (originalArticle) {
      const articleId = originalArticle.id;
      editArticle(articleId, article);
      history.push(getArticleUri(articleId, locale));
      return;
    }

    addArticle(article);
    history.push(getLocaleURL(Links.ADMIN));
  };

  return (
    <Formik
      initialValues={getInitialValues()}
      onSubmit={handleSubmit}
      validateOnChange={false}
      validateOnBlur
      validationSchema={schema}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleSubmit,
        setFieldValue,
      }) => (
        <form onSubmit={handleSubmit}>
          <FieldArray
            name="locales"
            validateOnChange={false}
            render={(arrayHelpers) => (
              <Tabs className="mb-3" activeKey={locale} onSelect={onSwitchTabs}>
                {LOCALES.map(({ title, shortHand }) => (
                  <Tab
                    key={shortHand}
                    eventKey={shortHand}
                    title={title}
                    tabClassName={getTabClass(errors, shortHand, locale)}
                  >
                    <ArticleBody language={shortHand} helpers={arrayHelpers} />
                  </Tab>
                ))}
              </Tabs>
            )}
          />
          <InputGroup label="Date: " isRequired>
            <>
              <ReactDatePicker
                name="date"
                onChange={(date) => setFieldValue('date', date)}
                selected={values.date}
              />
              {errors.date && touched.date && (
                <Alert className="mt-1" variant="danger">
                  {errors.date}
                </Alert>
              )}
            </>
          </InputGroup>
          <InputGroup label="" controlId="isActiveCheck">
            <Form.Check
              name="isActive"
              type="checkbox"
              label="Is Active"
              onChange={handleChange}
              value={values.isActive}
            />
          </InputGroup>
          <Row className="mb-3 justify-content-end">
            <Col sm="10">
              <Button variant="primary" type="submit">
                Submit
              </Button>
            </Col>
          </Row>
        </form>
      )}
    </Formik>
  );
};

ArticleForm.propTypes = {
  addArticle: PropTypes.func.isRequired,
  editArticle: PropTypes.func.isRequired,
  locale: PropTypes.oneOf(['en', 'de', 'bg']),
  history: PropTypes.shape({
    push: PropTypes.func,
    replace: PropTypes.func,
  }).isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
  article: PropTypes.any,
};

ArticleForm.defaultProps = {
  locale: 'en',
  article: null,
};

export default ArticleForm;

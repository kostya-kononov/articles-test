/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { Alert, Form } from 'react-bootstrap';
import Editor from 'jodit-react';
import InputGroup from 'components/InputGroup';

import styles from './ArticleBody.module.css';

const areEqual = (prevProps, nextProps) => {
  const { language: prevLang, helpers: { form: prevForm } } = prevProps;
  const { language: nextLang, helpers: { form: nextForm } } = nextProps;
  const prevValues = prevForm.values.locales[nextLang];
  const nextValues = nextForm.values.locales[nextLang];
  const prevErrors = prevForm.errors.locales ? prevForm.errors.locales[nextLang] : {};
  const nextErrors = nextForm.errors.locales ? nextForm.errors.locales[nextLang] : {};
  return prevLang === nextLang && prevValues === nextValues && prevErrors === nextErrors;
};

const ArticleBody = React.memo((props) => {
  const { language, helpers: { form } } = props;
  const values = form.values.locales[language];
  const errors = form.errors.locales ? form.errors.locales[language] : {};
  const touched = form.touched.locales ? form.touched.locales[language] : {};

  const onContentChanged = (value, event) => {
    const element = event.target;
    form.setFieldValue(`locales.${language}.content`, value);
    form.setFieldValue(
      `locales.${language}.preview`,
      element.innerText.slice(0, 50),
    );
  };

  return (
    <>
      <InputGroup label="Title: " isRequired>
        <>
          <Form.Control
            name={`locales.${language}.title`}
            type="text"
            placeholder="Enter article title"
            value={values.title}
            onChange={form.handleChange}
          />
          {errors.title && touched.title && (
            <Alert className="mt-1" variant="danger">
              {errors.title}
            </Alert>
          )}
        </>
      </InputGroup>
      <InputGroup label="Content: " isRequired>
        <Editor
          config={{ editorCssClass: styles.textarea }}
          value={values.content}
          onBlur={onContentChanged}
        />
        {errors.content && touched.content && (
        <Alert className="mt-1" variant="danger">
          {errors.content}
        </Alert>
        )}
      </InputGroup>
    </>
  );
}, areEqual);

ArticleBody.propTypes = {
  language: PropTypes.string,
  helpers: PropTypes.any.isRequired,
};

ArticleBody.defaultProps = {
  language: 'en',
};

export default ArticleBody;

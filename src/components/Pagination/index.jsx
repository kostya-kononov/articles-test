import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { getArticleUri } from 'helpers/locale';
import styles from './Pagination.module.css';

const Pagination = (props) => {
  const { articles, currentId, locale } = props;

  const getPaginatedIds = () => {
    const listedArticles = articles.filter((item) => !!item.isActive);
    const { length } = listedArticles;
    const arrayPos = listedArticles.reduce((total, current, index) => {
      if (current.id === currentId) return index;
      return total;
    }, 0);

    const previous = listedArticles[(arrayPos + length - 1) % length];
    const next = listedArticles[(arrayPos + 1) % length];
    return {
      previous: getArticleUri(previous.id, locale),
      next: getArticleUri(next.id, locale),
    };
  };

  const pagination = getPaginatedIds();
  return (
    <div className={styles.wrap}>
      <i className="bi bi-arrow-left-short" />
      <Link className={styles.link} to={pagination.previous}>Previous</Link>
      <i className="bi bi-dash" />
      <Link className={styles.link} to={pagination.next}>Next</Link>
      <i className="bi bi-arrow-right-short" />
    </div>
  );
};

Pagination.propTypes = {
  articles: PropTypes.any.isRequired,
  currentId: PropTypes.string.isRequired,
  locale: PropTypes.oneOf(['en', 'de', 'bg']),
};

Pagination.defaultProps = {
  locale: 'en',
};

export default Pagination;

import React from 'react';
import PropTypes from 'prop-types';
import { Button, Modal } from 'react-bootstrap';

const DeleteConfirmation = (props) => {
  const { visible, onSubmit, onClose } = props;

  return (
    <Modal show={visible}>
      <Modal.Body>
        <p>Are you sure you want to delete this record?</p>
      </Modal.Body>

      <Modal.Footer>
        <Button variant="primary" onClick={onSubmit}>Yes</Button>
        <Button variant="secondary" onClick={onClose}>No</Button>
      </Modal.Footer>
    </Modal>
  );
};

DeleteConfirmation.propTypes = {
  visible: PropTypes.bool,
  onSubmit: PropTypes.func,
  onClose: PropTypes.func,
};

DeleteConfirmation.defaultProps = {
  visible: false,
  onSubmit: () => {},
  onClose: () => {},
};

export default DeleteConfirmation;

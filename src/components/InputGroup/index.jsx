import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { Col, Form, Row } from 'react-bootstrap';
import styles from './InputGroup.module.css';

const InputGroup = (props) => {
  const {
    label,
    children,
    isRequired,
    ...rest
  } = props;
  const labelStyles = clsx('text-end fw-bold', isRequired && styles.requiredInput);

  return (
    <Form.Group as={Row} className="mb-3" {...rest}>
      <Form.Label column sm="2" className={labelStyles}>
        {label}
      </Form.Label>
      <Col sm="10">
        {children}
      </Col>
    </Form.Group>
  );
};

InputGroup.propTypes = {
  label: PropTypes.string,
  children: PropTypes.node.isRequired,
  isRequired: PropTypes.bool,
};

InputGroup.defaultProps = {
  label: '',
  isRequired: false,
};

export default InputGroup;

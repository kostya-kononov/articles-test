import React from 'react';
import PropTypes from 'prop-types';
import LocaleSelect from 'components/LocaleSelect';

const Header = (props) => {
  const { locale, history, location } = props;

  return (
    <header className="py-4">
      <div className="container d-flex justify-content-end">
        <LocaleSelect
          locale={locale}
          history={history}
          location={location}
        />
      </div>
    </header>
  );
};

Header.propTypes = {
  locale: PropTypes.oneOf(['en', 'de', 'bg']),
  history: PropTypes.shape({
    push: PropTypes.func,
    replace: PropTypes.func,
  }).isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
};

Header.defaultProps = {
  locale: 'en',
};

export default Header;

import React from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { LOCALES } from 'constants/global';

const LocaleWrapper = (WrappedComponent) => {
  const LocaleHOC = (props) => {
    const { match, location } = props;
    const { params: { lang } } = match;
    const isSupportedLocale = !!LOCALES.find((val) => val.shortHand === lang);

    if (!isSupportedLocale) {
      const redirectUrl = location.pathname.replace(lang, 'en');
      return <Redirect to={redirectUrl} />;
    }

    return (
      <WrappedComponent locale={lang} {...props} />
    );
  };

  LocaleHOC.propTypes = {
    match: PropTypes.shape({
      params: PropTypes.shape({
        lang: PropTypes.string,
      }),
    }).isRequired,
    location: PropTypes.shape({
      pathname: PropTypes.string,
    }).isRequired,
  };

  return LocaleHOC;
};

export default LocaleWrapper;

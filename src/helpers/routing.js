import HomePage from 'pages/Home';
import { Links } from 'constants/global';
import AddArticle from 'pages/AddArticle';
import ViewArticle from 'pages/ViewArticle';
import Admin from 'pages/Admin';

const routes = {
  HOME: {
    path: Links.HOME,
    exact: true,
    title: 'Home Page',
    component: HomePage,
  },
  ADD_ARTICLE: {
    path: Links.ADD_ARTICLE,
    title: 'Add Article',
    component: AddArticle,
  },
  EDIT_ARTICLE: {
    path: Links.EDIT_ARTICLE,
    title: 'View Article',
    component: AddArticle,
  },
  ADMIN: {
    path: Links.ADMIN,
    title: 'Manage articles',
    component: Admin,
  },
  VIEW_ARTICLE: {
    path: Links.VIEW_ARTICLE,
    title: 'View Article',
    component: ViewArticle,
  },
};

export default routes;

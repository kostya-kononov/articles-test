/* eslint-disable import/prefer-default-export */
export const getDateString = (date) => new Intl.DateTimeFormat('en-US', { dateStyle: 'long' }).format(date);

export const getDateShortString = (date) => new Intl.DateTimeFormat('en-GB', { dateStyle: 'short' }).format(date);

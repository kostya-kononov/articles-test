import { Links } from 'constants/global';

export const getLocaleURL = (url, locale) => url.replace(':lang', locale);

export const getArticleUri = (articleId, locale) => {
  const link = getLocaleURL(Links.VIEW_ARTICLE, locale);
  return link.replace(':id', articleId);
};

export const getEditArticleUri = (articleId, locale) => {
  const link = getLocaleURL(Links.EDIT_ARTICLE, locale);
  return link.replace(':id', articleId);
};

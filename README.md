# Articles Listing
A web app for listing articles from the browser's local storage.

### Running Project on your Local Machine
* `yarn`|`npm install` - install all necessary dependencies
* `yarn start`|`npm start` - start the local server

### Tech Stack
- [React](https://reactjs.org/docs/getting-started.html)
- [Redux](https://redux.js.org/)
- [Formik](https://formik.org/docs/overview)
- [Bootstrap](https://react-bootstrap.github.io/)
